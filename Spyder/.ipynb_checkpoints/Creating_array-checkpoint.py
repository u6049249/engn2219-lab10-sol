"""
### Before you continue ###
There are many ways to create these matrix, following solutions are just one way to create the corresponding matrix. 

To guarantee fast code, try your best to just use functions provided in numpy.

To understand the code, try print out the result of each line or parts of it.
"""

import numpy as np
import time

### First one ###
# bit smelly solution
x = np.eye(5)
y = np.fliplr(x)
((x+y)>0).astype(float)

# Breaking it down
print("x: \n", x, "\n")
print("y: \n", y, "\n")
print("x+y: \n", x+y, "\n")
print("(x+y)>0: \n", (x+y)>0, "\n")
print("((x+y)>0).astype(float): \n", ((x+y)>0).astype(float), "\n")


### Second one ###
x = np.arange(1,6)
np.diag(x, k=-1)[1:, 1:]

# Breaking it down
print(f"x: \n {x} \n")
print(f"np.diag(x, k=-1): \n{np.diag(x, k=-1)} \n")
print(f"np.diag(x, k=-1)[1:, 1:]: \n{np.diag(x, k=-1)[1:, 1:]} \n")



### Third one ###
This one is a bit hard, but try breaking it down yourself and try and see if you understand what is going on.

There are two solutions, the first one used python lists, while the second one just uses functions from numpy.

Try breaking down the solutions down and print them out. I will leave some code below to hint what is going on.


# Third one

# First solution:
# (np.array([np.arange(1,6),] * 5).T ** np.arange(5)).T

# Second solution (better one): 
(np.tile(np.arange(1,6), (5,1)).T ** np.arange(5)).T

### hints for third one ###
x = np.arange(25).reshape(5,5) # Create an array to use for example
print(f"x: \n{x}")

y = np.array([1,2,1,1,0])
print(f"y: \n{y}\n")
print(f"x ** y: \n{x ** y}")