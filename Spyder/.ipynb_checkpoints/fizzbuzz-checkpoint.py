def fizz_buzz(m):
    """
    Divisible by:
        3 : Fizz
        5 : Buzz
        Both: FizzBuzz
        Neither: Nothing
    """
    F = "Fizz"
    B = "Buzz"

    print(0)
    for i in range(1,m):
        string = ""
        div_3 = i % 3
        div_5 = i % 5

        if div_3 == 0:
            string += F
        if div_5 == 0:
            string += B
        
        print(f"{i} {string}")

fizz_buzz(20)