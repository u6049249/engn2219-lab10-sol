def countdown(n):
    while n > 0:
        yield n
        n -= 1
    return

c10 = countdown(10)
next(c10) # returns 10
next(c10) # returns 9
# for i in c10: 
# 	print(i**2) 
                                                                                            
next(c10) ** 2 # raises an exception?