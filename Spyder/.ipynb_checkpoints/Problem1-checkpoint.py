import numpy as np

def intersection_point(A, c):
    """
    The original formula is: A @ r = -c
    where r is a 2x1 matrix that contains the x and y
    coordinate of the intersection point.
    
    Hence from the formula above we could get:
                     A @ r = -c
            inv(A) @ A @ r = inv(A) @ -c
                     I @ r = inv(A) @ -c
                         r = inv(A) @ -c
                         
    where I is the identity matrix
    """
    return np.linalg.inv(A) @ -c

#Example:
A = np.array([[1,1], [2,1]])
c = np.array([1, 1]).reshape(2,1)

np.dot(np.linalg.inv(A), c)



def triangle_area(x):
    A12 = x[:2, :-1]
    c12 = x[:2, -1]
    
    A23 = x[1:3, :-1]
    c23 = x[1:3, -1]
    
    mod_x = np.delete(x, 1, axis=0)
    A13 = mod_x[:, :-1]
    c13 = mod_x[:, -1]
    
    # Calculate the intersection points
    A = intersection_point(A12,c12)
    B = intersection_point(A23,c23)
    C = intersection_point(A13,c13)
    
#     # Check if A, B and C are correct
#     print(A) 
#     print(B)
#     print(C)
    
    # Calculate the vectors
    AB = B-A
    AC = C-A
    
    return 0.5 * np.linalg.norm(np.cross(AB, AC))


# Test
test = np.array([[0, 1, 0], 
                 [1, -1, 0], 
                 [1, 1, -2]])
print(f"Area of triangle is {triangle_area(test)}")


