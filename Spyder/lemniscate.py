#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot  as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

a = 1.0
b = np.sqrt(2)*a
delta = a / 100
x = np.arange(-2.5*a, 2.5*a, delta )
y = np.arange(-2.5*a, 2.5*a, delta )
X, Y = np.meshgrid(x, y)
Z = (X**2 + Y**2)**2 - 2*a**2 * (X**2 - Y**2)

# levels = np.arange(-0.25*a, 0.1*a, 0.01)

fig = plt.figure()
ax = fig.gca(projection='3d')

#yeet

# fig, ax = plt.subplots()
# ax = Axes3D(fig)
ax.plot_surface(X, Y, Z, rstride=8, cstride=8, alpha=0.3)
cset = ax.contour(X, Y, Z, zdir='z', cmap=cm.coolwarm, offset=-50) 

ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')

ax.set_title('Bernoulli Lemniscate function and its projection curve')

# CS = ax2.contour(X, Y, Z)
# manual_locations = [(0.25*a, 0.25*a), (1.5*a, 1.5*a)]
# # # [(-1, -1.4), (-0.62, -0.7), (-2, 0.5), (1.7, 1.2), (2.0, 1.4), (2.4, 1.7)]
# ax2.clabel(CS, inline=1, fontsize=7, manual=manual_locations)
# ax2.set_title('Contour plot of Bernoulli Lemniscate function')


plt.show()