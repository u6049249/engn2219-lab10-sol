import numpy as np

arr = np.arange(25).reshape(5,5) # <-creates a 5x5 matrix, which values from 0 to 24
print(f"arr: \n{arr}\n")

# Access element at xth row, yth column
# arr[x, y]
print(arr[3, 2])


# Creating matrix using functions
fn = lambda x,y: 10*x + y                                                                      
a = np.fromfunction(fn,(6,6),dtype=np.int)
print(f" **1** {a}")

x = a[a%2 == 0] # will return a flattened array with only even elements
print(f"**2** {x}")

x = a[a%2 == 0] = -1  # which can be assigned a new value based on their common property           
print(f"**3** {x}")